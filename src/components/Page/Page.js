import React, {useEffect, useState} from 'react';
import './Page.css';
import {Preloader, Puff} from "react-preloader-icon";
import axios from "axios";

const Page = ({match}) => {

   const [page, setPage] = useState(null);
   useEffect(() => {
      const requestPage = async () => {
         const {data} = await axios.get(match.params.page + '.json');
         setPage(data)
      };
      setPage(null);
      requestPage();
   }, [match]);

   return (
     <div className="quotes-block">
        {page ? (
          <div>
             <h3>{page.title}</h3>
             <p>{page.content}</p>
          </div>
        ) : (
          <div style={
             {
                position: 'absolute',
                left: '0',
                top: '0',
                display: 'flex',
                zIndex: '1000',
                background: 'rgba(239,238,238,0.7)',
                width: '100%',
                height: '100%',
             }}>
             <div style={{margin: 'auto'}}>
                <Preloader
                  use={Puff}
                  size={132}
                  strokeWidth={10}
                  strokeColor="black"
                  duration={1200}
                />
             </div>
          </div>
        )}
     </div>
   )
};

export default Page;