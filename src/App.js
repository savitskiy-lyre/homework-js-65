import {BrowserRouter, Route, Switch} from "react-router-dom";
import Layout from "./components/UI/Layout/Layout";
import './App.css';
import ChangePage from "./components/ChangePage/ChangePage";
import Page from "./components/Page/Page";
import axios from "axios";
import {BASE_URL} from "./config";
import {useEffect, useState} from "react";
import {Bars, Preloader} from "react-preloader-icon";

axios.defaults.baseURL = BASE_URL;

const App = () => {
   const [pages, setPages] = useState(null);
   useEffect(() => {
      const requestingPages = async () => {
         const {data} = await axios.get('.json');
         setPages(data);
         //console.log(data);
      };
      requestingPages();
   }, [])
   return (
     <BrowserRouter>
        <div className="App">
           <Layout data={pages}>
              {pages ? (
              <Switch>
                 <Route path='/pages/admin' component={({history}) => <ChangePage history={history} pages={pages}/>}/>
                 <Route path='/pages/:page' component={Page}/>
              </Switch>
              ) : (
                <div style={
                   {
                      position: 'absolute',
                      left: '0',
                      top: '0',
                      display: 'flex',
                      zIndex: '1000',
                      background: 'rgba(239,238,238,0.7)',
                      width: '100%',
                      height: '100%',
                   }}>
                   <div style={{margin: 'auto'}}>
                      <Preloader
                        use={Bars}
                        size={132}
                        strokeWidth={10}
                        strokeColor="black"
                        duration={1200}
                      />
                   </div>
                </div>

              )}
           </Layout>
        </div>
     </BrowserRouter>

   );
};

export default App;
