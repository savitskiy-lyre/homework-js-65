import React from 'react';
import {NavLink} from 'react-router-dom';
import './NavBar.css';

const NavBar = ({data}) => {
   const activeNavItem = {
      color: 'orange',
      textDecoration: 'underline',
   }
   return data && (
     <div className='NavBar'>
        <h3>Pages</h3>
        <ul className='nav-wrapper'>
           {Object.keys(data).map((key) => {
              return (
                <li key={Math.random()}>
                   <NavLink
                     exact
                     to={'/pages/' + key}
                     activeStyle={activeNavItem}
                   >
                      {key[0].toUpperCase() + key.split('').splice(1).join('')}
                   </NavLink>
                </li>
              );
           })}
           <li>
              <NavLink exact to={'/pages/admin'} activeStyle={activeNavItem}>Admin</NavLink>
           </li>
        </ul>
     </div>
   );
};

export default NavBar;