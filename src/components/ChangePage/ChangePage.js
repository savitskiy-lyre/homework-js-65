import React, {useEffect, useState} from 'react';
import axios from "axios";
import './ChangePage.scss';
import {Bars, Preloader} from "react-preloader-icon";

const ChangePage = ({pages, history}) => {
   const [pageVal, setPageVal] = useState('');
   const [titleVal, setTitleVal] = useState('');
   const [contentVal, setContentVal] = useState('');
   const [isRequesting, setIsRequesting] = useState(false);
   const onSubmitPage = (e) => {
      e.preventDefault();
      const replacePage = async () => {
         await axios.put(pageVal + '.json', {title: titleVal, content: contentVal});
         history.replace('/pages/' + pageVal);
      };
      replacePage();
   }
   useEffect(() => {
      if (pageVal) {
         const requestPage = async () => {
            const {data} = await axios.get(pageVal + '.json');
            setTitleVal(data.title);
            setContentVal(data.content);
            setIsRequesting(false);
         };
         setIsRequesting(true);
         requestPage();
      }
   }, [pageVal])

   return (
     <div className="change-quote-block">
        <form className='change-quote-form' onSubmit={onSubmitPage}>
           {isRequesting && (
             <div style={
                {
                   position: 'absolute',
                   left: '0',
                   top: '0',
                   display: 'flex',
                   zIndex: '1000',
                   background: 'rgba(239,238,238,0.7)',
                   width: '100%',
                   height: '100%',
                }}>
                <div style={{margin: 'auto'}}>
                   <Preloader
                     use={Bars}
                     size={132}
                     strokeWidth={10}
                     strokeColor="black"
                     duration={1200}
                   />
                </div>
             </div>)}
           <div className="label">
              Page
           </div>
           <select value={pageVal} onChange={(e) => setPageVal(e.target.value)} required>
              {Object.keys(pages).map((key) => {
                 return (
                   <option key={key + pages[key].title}>{key}</option>
                 )
              })}
              <option/>
           </select>
           <div className="label">
              Title
           </div>
           <input type="text" value={titleVal} onChange={e => setTitleVal(e.target.value)} required/>
           <div className="label">
              Description
           </div>
           <textarea value={contentVal} onChange={e => setContentVal(e.target.value)} cols="30" rows="10" required/>
           <button className='submit-btn'>Confirm</button>
        </form>
     </div>
   );
};

export default ChangePage;