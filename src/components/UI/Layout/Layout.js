import React from 'react';
import NavBar from "../../NavBar/NavBar";
import './Layout.css';

const Layout = ({children, data}) => {
   return (
     <>
        <NavBar data={data}/>
        <main className="Layout-content">
           {children}
        </main>
     </>
   );
};

export default Layout;